// import * as React from 'react';
import React, { Component, createRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SplashScreen } from './src/screen/SplashScreen'
import { OnboardingScreen } from './src/screen/OnboardingScreen'
import { SignInScreen } from './src/screen/SignInScreen'
import { SignUpScreen } from './src/screen/SignUpScreen'
import { VerifyPhoneScreen } from './src/screen/VerifyPhoneScreen'
import { VerificationScreen } from './src/screen/VerificationScreen'
import { DeliciousScreen } from './src/screen/DeliciousScreen'
import { MostPopularScreen } from './src/screen/MostPopularScreen'
import { FilterScreen } from './src/screen/FilterScreen'
import { OrderScreen } from './src/screen/OrderScreen'
import { PaymentScreen } from './src/screen/PaymentScreen'
import { PaymentProcessScreen } from './src/screen/PaymentProcessScreen'
import { OrderCartScreen } from './src/screen/OrderCartScreen'
import ScalingDrawer from 'react-native-scaling-drawer';
import { LeftMenu } from './src/component/LeftMenu';
import { wp } from './src/helper/responsiveScreen';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

const Stack = createStackNavigator();

export const drawer = createRef();

const defaultScalingDrawerConfig = {
  scalingFactor: 0.8,
  minimizeFactor: 0.6,
  swipeOffset: 10,
};

function HomeTabs() {
  return (
    <ScalingDrawer
      ref={drawer}
      content={<LeftMenu drawer={drawer} />}
      {...defaultScalingDrawerConfig}
      onClose={() => console.log('close')}
      onOpen={() => console.log('open')}
    >
      <Drawer.Navigator initialRouteName="DeliciousScreen" screenOptions={{
        headerShown: false
      }}>
        <Drawer.Screen name="DeliciousScreen" component={DeliciousScreen} />
        <Drawer.Screen name="MostPopularScreen" component={MostPopularScreen} />
        <Drawer.Screen name="FilterScreen" component={FilterScreen} />
        <Drawer.Screen name="OrderScreen" component={OrderScreen} />
        <Drawer.Screen name="PaymentScreen" component={PaymentScreen} />
        <Drawer.Screen name="PaymentProcessScreen" component={PaymentProcessScreen} />
        <Drawer.Screen name="OrderCartScreen" component={OrderCartScreen} />
      </Drawer.Navigator>
    </ScalingDrawer>
  );
}

function App() {

  return (

    <NavigationContainer>
      <Stack.Navigator initialRouteName="SplashScreen" screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="OnboardingScreen" component={OnboardingScreen} />
        <Stack.Screen name="SignInScreen" component={SignInScreen} />
        <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
        <Stack.Screen name="VerifyPhoneScreen" component={VerifyPhoneScreen} />
        <Stack.Screen name="VerificationScreen" component={VerificationScreen} />
        <Stack.Screen name="DeliciousScreen" component={HomeTabs} />
        
      </Stack.Navigator>

    </NavigationContainer>


  );
}

export default App;