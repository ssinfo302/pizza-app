import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native';
// import NavigationService from './NavigationService';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'

class LeftMenu extends Component {

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Constant.color.background }}>
        <View style={{ marginLeft: wp(5), marginTop: Platform.OS == 'ios' ? hp(11) : hp(7) }}>
          <Image
            source={require('../img/profile.png')}
            style={{ height: wp(15), width: wp(15), borderRedius: wp(5), }} />
          <Text style={{ ...styles.text, marginTop: hp(2) }}>Jimmy Clark</Text>

          <TouchableOpacity onPress={() => {
            // this.props.navigation.navigate('DeliciousScreen')
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(6) }}>
              <Image
                source={require('../img/home.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Home</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            // this.props.navigation.navigate('OrderScreen')
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(4) }}>
              <Image
                source={require('../img/setting.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Setting</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            // this.props.navigation.navigate('OrderScreen')
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(4) }}>
              <Image
                source={require('../img/orders.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Orders</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(4) }}>
              <Image
                source={require('../img/notification.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Notif<Text style={{ ...styles.text, marginLeft: wp(5), }}>ication</Text></Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(4) }}>
              <Image
                source={require('../img/payment.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Payment Method</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(4) }}>
              <Image
                source={require('../img/help.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Help Center</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {
            this.props.drawer.current.close();
          }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp(4) }}>
              <Image
                source={require('../img/logout.png')}
                style={{ height: wp(6), width: wp(6) }} />
              <Text style={{ ...styles.text, marginLeft: wp(5), }}>Logout</Text>
            </View>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

export { LeftMenu }

const styles = StyleSheet.create({
  text: {
    color: Constant.color.white,
    fontSize: Constant.fontSize.medium,
    fontFamily: "NexaDemo-Bold"
  },
});
