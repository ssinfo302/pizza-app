import React, { Component } from 'react';
import {
  StyleSheet,       // CSS-like styles
  Text,             // Renders text
  TouchableOpacity, // Pressable container
  View              // Container component
} from 'react-native';
import Constant from '../helper/themeHelper'
import { wp } from '../helper/responsiveScreen';


export default class Button extends Component {
  render({ onPress } = this.props) {
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.button}>
          <Text style={styles.text}>{this.props.text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 12,         
    backgroundColor: Constant.color.background,   
    width: wp(80),    
    paddingVertical: 12,
    alignItems:'center'
  },
  text: {
    fontSize: Constant.fontSize.medium,
    color: Constant.color.white,
    fontFamily: 'NexaDemo-Bold'
  },
});