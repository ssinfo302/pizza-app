import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import { drawer } from "../../App";

class OrderCartScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }



    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.header}>
                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }}
                        onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5) }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{'Order'} </Text>

                </View>

                <View style={{ marginLeft: wp(7), marginTop: hp(12), marginRight: wp(7) }}>
                    <View style={{ alignItems: 'center' }}>
                        <Image
                            source={require('../img/cart.png')} />
                        <Text style={styles.text}>Your Orders Cart is Empty.</Text>
                        <Text style={{
                            color: Constant.color.lightGray,
                            fontSize: Constant.fontSize.xsmall,
                            fontFamily: "NexaDemo-Bold",
                            marginTop: hp(1),
                            textAlign: 'center'
                        }}>{`Good food is always cooking! Go Ahead \n order some Testy Food.`}</Text>
                    </View>

                   
                            <TouchableOpacity style={styles.button} onPress={() => this.setState({ isModalVisible: true })}>
                                <Text style={styles.btnText}>Order Now</Text>
                            </TouchableOpacity>
                       
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    text: {
        color: Constant.color.black,
        fontSize: Constant.fontSize.large,
        fontFamily: "NexaDemo-Bold",
        marginTop: hp(8)
    },
    button: {
        borderRadius: wp(7),
        backgroundColor: Constant.color.background,
        marginLeft: wp(17),
        marginRight: wp(17),
        marginTop: hp(7),
        paddingVertical: 12,
        alignItems: 'center'
    },
    btnText: {
        fontSize: Constant.fontSize.xmedium,
        color: Constant.color.white,
        fontFamily: 'NexaDemo-Bold'
    },
});

export { OrderCartScreen }