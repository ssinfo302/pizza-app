import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, FlatList, ScrollView, ImageBackground } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import Button from '../component/Button';
import { SliderBox } from "react-native-image-slider-box";
import { drawer } from "../../App";

const MPData = [
    {
        image: require('../img/mp_1.png'),
        name: 'Cheezy Tomato',
        restaurantName: 'Will’s Cafe',
        price: '$60',
        rate: 4,
        isRestaurant: false
    },
    {
        image: require('../img/mp_2.png'),
        name: 'Spicy Corn',
        restaurantName: 'Johnson Pizza',
        price: '$80',
        rate: 3,
        isRestaurant: false
    },
    {
        image: require('../img/mp_1.png'),
        name: 'Margreta',
        restaurantName: 'D’s Pizza cafe',
        price: '$120',
        rate: 4,
        isRestaurant: false
    },
    {
        image: require('../img/mp_2.png'),
        name: '8 ‘s cornfin',
        restaurantName: 'William pizza studio',
        price: '$80',
        rate: 3,
        isRestaurant: false
    },
];

const PRData = [
    {
        image: require('../img/pr_1.png'),
        name: 'N - Y Pizza hub',
        restaurantName: 'Smith Johns',
        price: '',
        rate: 4,
        isRestaurant: true
    },
    {
        image: require('../img/pr_2.png'),
        name: 'Spyker Pizza House',
        restaurantName: 'Tony Z',
        price: '',
        rate: 3,
        isRestaurant: true
    },
    {
        image: require('../img/pr_1.png'),
        name: 'Mincha cafe',
        restaurantName: 'Mark jems',
        price: '',
        rate: 4,
        isRestaurant: true
    },
    {
        image: require('../img/pr_2.png'),
        name: 'Domino’z Pizza',
        restaurantName: 'Luis Worth',
        price: '',
        rate: 3,
        isRestaurant: true
    },
];

// const CPData = [
//     {
//         image: require('../img/cp_1.png'),
//         name: 'Fun burn frie',
//         desc: '2 slice'
//     },
//     {
//         image: require('../img/cp_2.png'),
//         name: 'Maxican pizza',
//         desc: '2 flows with pepsi'
//     },
//     {
//         image: require('../img/cp_1.png'),
//         name: 'Mac Veggies',
//         desc: 'Box Crunch'
//     },
//     {
//         image: require('../img/cp_2.png'),
//         name: 'Black Stone Paper',
//         desc: 'Bunch Break'
//     },
// ];

const CPData = [
    {
        image: require('../img/cp_3.png'),
    },
    {
        image: require('../img/cp_4.png'),
    },
    {
        image: require('../img/cp_3.png'),
    },
    {
        image: require('../img/cp_4.png'),
    },
];

class DeliciousScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            images: [
                require('../img/ad.png'),
                require('../img/ad.png'),
                require('../img/ad.png'),
                require('../img/ad.png'),
            ]
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        BackHandler.exitApp()
        return true
    }

    renderMostPopularItem = (item, index) => {
        const starIcon = require('../img/star.png')
        const emptyIcon = require('../img/empty_star.png')

        let React_Native_Rating_Bar = []
        for (var i = 1; i <= 5; i++) {
            React_Native_Rating_Bar.push(
                <Image
                    style={styles.iconStyle}
                    source={i <= item.item.rate ? starIcon : emptyIcon}
                />
            )
        }
        return (
            <TouchableOpacity onPress={() => item.item.isRestaurant ? this.props.navigation.navigate('MostPopularScreen', {
                name: item.item.name,
            }) : null} >
                <View style={{
                    marginRight: wp(7), width: wp(38), height: hp(20), background: Constant.color.white, borderBottomRightRadius: wp(0.5),
                    borderBottomLeftRadius: wp(0.5), borderColor: '#18233520', borderWidth: wp(0.08), shadowColor: '#18233520',
                    shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.29, shadowRadius: 1.65, elevation: 0.5
                }}>
                    <Image
                        source={item.item.image}
                        style={{ height: Platform.OS == 'ios' ? Constant.isX ? hp(12.5) : hp(12) : hp(12.5), width: wp(38), }} />
                    <Text style={{ paddingLeft: wp(2), paddingTop: hp(0.6), color: Constant.color.black, fontSize: Constant.fontSize.xxxsmall, fontFamily: "NexaDemo-Bold" }}>
                        {item.item.name}</Text>
                    <View style={{
                        flexDirection: 'row', justifyContent: 'space-between', paddingLeft: wp(2),
                        paddingTop: Platform.OS == 'ios' ? Constant.isX ? hp(0.3) : hp(0.1) : hp(0.6), paddingRight: wp(2)
                    }}>
                        <Text style={{ color: '#787878', fontSize: Constant.fontSize.xmini, fontFamily: "NexaDemo-Bold" }}>{item.item.restaurantName}</Text>
                        <Text style={{ color: '#787878', fontSize: Constant.fontSize.xmini, fontFamily: "NexaDemo-Bold" }}>{item.item.price}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingLeft: wp(2), paddingTop: Platform.OS == 'ios' ? Constant.isX ? hp(0.4) : hp(0.1) : hp(0.6) }}>{React_Native_Rating_Bar}</View>

                </View>
            </TouchableOpacity>
        )
    }

    // renderComboPacksItem = (item, index) => {
    //     return (
    // <View style={{
    //     marginRight: wp(7), width: wp(38), height: hp(23)
    // }}>
    //     <ImageBackground
    //         source={item.item.image}
    //         style={{ height: hp(23), width: wp(38), }} >

    //         <Text style={{ color: Constant.color.white, marginTop: hp(15), marginLeft: wp(3), fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold"}}>
    //             {item.item.name}</Text>
    //         <View style={styles.seperateLine} />
    //         <Text style={{ color: '#FFFFFF95', marginTop: hp(0.5), marginLeft: wp(3), fontSize: Constant.fontSize.xxxsmall, fontFamily: "NexaDemo-Bold" }}>
    //             {item.item.desc}</Text>
    //     </ImageBackground>

    // </View>
    //     )
    // }

    renderComboPacksItem = (item, index) => {
        return (
            <View style={{
                marginRight: wp(7), width: wp(38), height: hp(22)
            }}>
                <Image
                    source={item.item.image}
                    style={{ height: hp(22), width: wp(38), }} />
            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.header}>
                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }}
                        onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5), }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{'Delicious!'} </Text>

                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0), right: 0, marginRight: wp(5) }}
                        onPress={() => this.props.navigation.navigate('FilterScreen')} >
                        <Image
                            source={require('../img/filter.png')}
                            style={{ height: hp(2.5), width: wp(5), }} />
                    </TouchableOpacity>
                </View>

                <View style={{ marginLeft: wp(5), marginRight: wp(5), marginTop: hp(2), marginBottom: hp(2), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                    <View style={{ flexDirection: 'row', height: hp(5) }}>
                        <TextInput
                            style={{ flex: 1, paddingLeft: wp(5), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.xxsmall, paddingRight: wp(14) }}
                            placeholder={'Search Your Food'}
                            placeholderTextColor={Constant.color.lightGray}
                            onChangeText={search => this.setState({ search })}
                        />
                        <View style={{ position: 'absolute', right: 0, flexDirection: 'row' }}>
                            <View style={styles.horizontalLine} />
                            <TouchableOpacity style={{ paddingTop: hp(1), paddingRight: wp(4) }} >
                                <Image source={require('../img/search.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ScrollView >
                    <SliderBox
                        images={this.state.images}
                        sliderBoxHeight={Platform.OS == 'ios' ? 217 : hp(30)}
                        dotColor={Constant.color.background}
                        inactiveDotColor={Constant.color.white}
                        dotStyle={{
                            width: wp(1.5),
                            height: wp(1.5),
                            borderRadius: wp(5),
                        }} />

                    <View style={{ flexDirection: 'row', marginLeft: wp(7), marginRight: wp(7), marginTop: hp(2), marginBottom: hp(1), justifyContent: 'space-between', }}>
                        <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold" }}>Most Popular</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('MostPopularScreen')}>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold" }}>See all</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginLeft: wp(7) }}>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={MPData}
                            renderItem={(item, index) => this.renderMostPopularItem(item, index)}
                            keyExtractor={item => item.name.toString()} />
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: wp(7), marginRight: wp(7), marginTop: hp(3), marginBottom: hp(1), justifyContent: 'space-between', }}>
                        <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold" }}>Populer Restaurants</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('MostPopularScreen')}>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold" }}>See all</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginLeft: wp(7) }}>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={PRData}
                            renderItem={(item, index) => this.renderMostPopularItem(item, index)}
                            keyExtractor={item => item.name.toString()} />
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: wp(7), marginRight: wp(7), marginTop: hp(3), marginBottom: hp(1), justifyContent: 'space-between', }}>
                        <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold" }}>Combo packs</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('MostPopularScreen')}>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold" }}>See all</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginLeft: wp(7), marginBottom: hp(5) }}>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={CPData}
                            renderItem={(item, index) => this.renderComboPacksItem(item, index)}
                            keyExtractor={item => item.image.toString()} />
                    </View>

                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    horizontalLine: {
        backgroundColor: Constant.color.lightGray,
        borderRadius: wp(3),
        width: wp(0.5),
        height: hp(3),
        marginRight: wp(3),
        marginTop: hp(1)
    },
    iconStyle: {
        width: Platform.OS == 'ios' ? Constant.isX ? wp(2.5) : wp(2) : wp(2.5),
        height: Platform.OS == 'ios' ? Constant.isX ? wp(2.5) : wp(2) : wp(2.5),
        resizeMode: 'cover',
        marginRight: wp(0.5)
    },
    seperateLine: {
        backgroundColor: Constant.color.background,
        borderRadius: wp(1.5),
        width: wp(6),
        height: hp(0.3),
        marginTop: hp(0.2),
        marginLeft: wp(3)
    }
});

export { DeliciousScreen }