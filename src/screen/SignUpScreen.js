import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, ScrollView } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import Button from '../component/Button';

class SignUpScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            email:'',
            phoneNumber:'',
            address:'',
            isPassword: true,
            checked: false,
        }
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('dark-content', true)
        StatusBar.setBackgroundColor(Constant.color.white)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        BackHandler.exitApp()
        return true
    }

    render() {
        const viewIcon = require('../img/eye.png');
        const noViewIcon = require('../img/hide.png');
        const checked = require('../img/uncheck.png');
        const unchecked = require('../img/check.png');

        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <ScrollView >

                    <View style={styles.conatinarLeft}>
                        <Image source={require('../img/left_side_pizza.png')} style={styles.Img} resizeMode={'contain'} />
                    </View>
                    <View style={styles.conatinarRight}>
                        <Image source={require('../img/right_side_pizza.png')} style={styles.Img} resizeMode={'contain'} />
                    </View>

                    <View style={{ marginTop: Platform.OS == 'ios' ? hp(20) : hp(16), marginBottom: hp(2), alignItems: 'center', }}>
                        <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxlarge, fontFamily: "NexaDemo-Bold" }}>
                            Sign Up</Text>
                        <View style={styles.seperateLine} />
                    </View>

                    <View style={{ margin: wp(5), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ paddingLeft: wp(5), paddingTop: wp(3.5), paddingBottom: wp(3.5), paddingRight: wp(2) }}>
                                <Image source={require('../img/user.png')} resizeMode={"contain"} />
                            </View>
                            <TextInput
                                style={{ flex: 1, marginTop: hp(0.3), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.small }}
                                placeholder={'User Name'}
                                placeholderTextColor={Constant.color.lightGray}
                                onChangeText={userName => this.setState({ userName })}
                            />
                        </View>
                    </View>

                    <View style={{ marginLeft: wp(5), marginRight: wp(5), marginBottom: wp(5), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ paddingLeft: wp(5), paddingTop: wp(3.5), paddingBottom: wp(3.5), paddingRight: wp(2) }}>
                                <Image source={require('../img/mail.png')} resizeMode={"contain"} />
                            </View>
                            <TextInput
                                style={{ flex: 1, marginTop: hp(0.3), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.small }}
                                placeholder={'E-mail'}
                                placeholderTextColor={Constant.color.lightGray}
                                onChangeText={email => this.setState({ email })}
                            />
                        </View>
                    </View>

                    <View style={{ marginLeft: wp(5), marginRight: wp(5), marginBottom: wp(5), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ paddingLeft: wp(5), paddingTop: wp(3.5), paddingBottom: wp(3.5), paddingRight: wp(2) }}>
                                <Image source={require('../img/phone.png')} resizeMode={"contain"} />
                            </View>
                            <TextInput
                                style={{ flex: 1, marginTop: hp(0.3), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.small }}
                                placeholder={'Phone number'}
                                keyboardType={'numeric'}
                                maxLength={10}
                                placeholderTextColor={Constant.color.lightGray}
                                onChangeText={phoneNumber => this.setState({ phoneNumber })}
                            />
                        </View>
                    </View>

                    <View style={{ marginLeft: wp(5), marginRight: wp(5), marginBottom: wp(5), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ paddingLeft: wp(5), paddingTop: wp(3.5), paddingBottom: wp(3.5), paddingRight: wp(2) }}>
                                <Image source={require('../img/address.png')} resizeMode={"contain"} />
                            </View>
                            <TextInput
                                style={{ flex: 1, marginTop: hp(0.3), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.small }}
                                placeholder={'Address'}
                                placeholderTextColor={Constant.color.lightGray}
                                onChangeText={address => this.setState({ address })}
                            />
                        </View>
                    </View>

                    <View style={{ marginLeft: wp(5), marginRight: wp(5), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ paddingLeft: wp(5), paddingTop: wp(3.5), paddingBottom: wp(3.5), paddingRight: wp(2) }}>
                                <Image source={require('../img/lock.png')} resizeMode={"contain"} />
                            </View>
                            <TextInput
                                style={{ flex: 1, marginTop: hp(0.3), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.small, paddingRight: wp(14) }}
                                placeholder={'Password'}
                                secureTextEntry={this.state.isPassword}
                                placeholderTextColor={Constant.color.lightGray}
                                onChangeText={password => this.setState({ password })}
                            />
                            <TouchableOpacity style={{ position: 'absolute', right: 0, paddingTop: wp(4), paddingBottom: wp(4), paddingRight: wp(8) }}
                                onPress={() => this.setState({ isPassword: !this.state.isPassword })} >
                                <Image source={this.state.isPassword ? noViewIcon : viewIcon} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: hp(3), marginLeft: wp(8) }}>
                        <TouchableOpacity onPress={() => this.setState({ checked: !this.state.checked })} >
                            <Image source={this.state.checked ? unchecked : checked} resizeMode={"contain"} />
                        </TouchableOpacity>
                        <Text style={{ marginLeft: wp(2), marginTop: wp(0.3), color: Constant.color.black, fontSize: Constant.fontSize.xxsmall, fontFamily: "NexaDemo-Bold" }}>Terms and condition</Text>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: hp(5) }}>
                        <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxsmall, fontFamily: "NexaDemo-Bold" }}>Or Sign Up With</Text>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: hp(2) }}>
                        <View style={{ flexDirection: 'row', }}>
                            <Image source={require('../img/google.png')} style={{ width: wp(12), height: hp(7), marginRight: wp(5) }} resizeMode={"contain"} />
                            <Image source={require('../img/facebook.png')} style={{ width: wp(12), height: hp(7) }} resizeMode={"contain"} />
                        </View>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: hp(5) }}>
                        <Button text="Sign Up" onPress={() => this.props.navigation.navigate('VerifyPhoneScreen')} />
                    </View>

                    <View style={{ alignItems: 'center', marginTop: hp(3), marginBottom: hp(5) }}>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxsmall, fontFamily: "NexaDemo-Bold" }}>
                                Already have an account?</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('SignInScreen')}>
                                <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xxsmall, fontFamily: "NexaDemo-Bold" }}> Login</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    ImgLeft: {
        width: wp(35),
        height: hp(25)
    },
    conatinarLeft: {
        flex: 1,
        position: 'absolute',
        left: 0,
        marginTop: Platform.OS == 'ios' ? hp(5.5) : hp(1.5)
    },
    ImgRight: {
        width: wp(25),
        height: hp(25)
    },
    conatinarRight: {
        flex: 1,
        position: 'absolute',
        right: 0,
        marginTop: Platform.OS == 'ios' ? hp(7.5) : hp(3.5)
    },
    seperateLine: {
        backgroundColor: Constant.color.background,
        borderRadius: wp(1.5),
        width: wp(10),
        height: hp(0.5),
        marginTop: hp(0.5),
        marginRight: wp(14)
    }
});

export { SignUpScreen }