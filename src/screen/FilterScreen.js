import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, ScrollView, FlatList, Dimensions } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomMarker from '../helper/CustomMarker';
import { drawer } from "../../App";

const d = Dimensions.get('window')
const SortData = [
    {
        name: 'Near by me',
    },
    {
        name: 'Top Rated',
    },
    {
        name: 'Cost High to low',
    },
    {
        name: 'Cost Low to High',
    },
    {
        name: 'Most Popular',
    },
];
const CuisinData = [
    {
        name: 'Cheeze corn',
    },
    {
        name: 'Black role',
    },
    {
        name: 'Toping',
    },
    {
        name: 'Pizza',
    },
    {
        name: 'Desserts',
    },
    {
        name: 'Maxican Pizza',
    },
];

class FilterScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            sortValue: [],
            cuisinValue: [],
            MultiSliderValue: [0, 70]
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    renderSortItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => this.onPressSort(item.item.name)} >
                <Text style={{
                    color: this.state.sortValue.length != 0 &&
                        this.state.sortValue.some(function (el) {
                            return el === item.item.name
                        })
                        ? Constant.color.background
                        : Constant.color.lightGray,
                    fontSize: Constant.fontSize.small,
                    fontFamily: "NexaDemo-Bold"
                }}>{item.item.name}</Text>

                {this.state.sortValue.length != 0 &&
                    this.state.sortValue.some(function (el) {
                        return el === item.item.name
                    })
                    ?
                    <Image
                        style={{ position: 'absolute', right: 0, marginRight: wp(7), marginTop: hp(0.5) }}
                        source={require('../img/sort_checkmark.png')}
                    />
                    : null}
                <View style={styles.seperateLine} />
            </TouchableOpacity>
        )
    }

    onPressSort(value) {

        let exist = this.state.sortValue.some(function (el) {
            return el === value
        })
        if (!exist) {
            this.setState(prevState => ({
                sortValue: [...prevState.sortValue, value]
            }));
            this.forceUpdate()
        } else {
            var removeIndex = this.state.sortValue
                .map(function (item) {
                    return item
                })
                .indexOf(value)
            this.state.sortValue.splice(removeIndex, 1)
            this.forceUpdate()
        }
    }

    renderCuisinItem = (item, index) => {
        return (
            <TouchableOpacity
                style={{
                    borderRadius: wp(8),
                    borderColor: this.state.cuisinValue.length != 0 &&
                        this.state.cuisinValue.some(function (el) {
                            return el === item.item.name
                        })
                        ? Constant.color.background
                        : '#808080',
                    borderWidth: wp(0.3),
                    paddingVertical: wp(2),
                    paddingHorizontal: wp(5),
                    alignItems: 'center',
                    marginTop: hp(1.5),
                    marginBottom: hp(0.5),
                    marginRight: wp(3)
                }}
                onPress={() => this.onPressCusin(item.item.name)} >
                <Text
                    style={{
                        fontSize: Constant.fontSize.xsmall,
                        color: this.state.cuisinValue.length != 0 &&
                            this.state.cuisinValue.some(function (el) {
                                return el === item.item.name
                            })
                            ? Constant.color.background
                            : '#808080',
                        fontFamily: 'NexaDemo-Bold'
                    }}>{item.item.name}</Text>
            </TouchableOpacity>
        )
    }

    onPressCusin(value) {

        let exist = this.state.cuisinValue.some(function (el) {
            return el === value
        })
        if (!exist) {
            this.setState(prevState => ({
                cuisinValue: [...prevState.cuisinValue, value]
            }));
            this.forceUpdate()
        } else {
            var removeIndex = this.state.cuisinValue
                .map(function (item) {
                    return item
                })
                .indexOf(value)
            this.state.cuisinValue.splice(removeIndex, 1)
            this.forceUpdate()
        }
    }

    render() {
        const starIcon = require('../img/star.png')
        const emptyIcon = require('../img/empty_star.png')
        const left = this.state.MultiSliderValue[0] * (d.width - 75) / 200 - 25;
        const left1 = this.state.MultiSliderValue[1] * (d.width - 75) / 200 - 75;

        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>

                <View style={styles.header}>
                    <TouchableOpacity 
                    style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }} 
                    onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5), }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{'Filter'} </Text>

                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0), right: 0, marginRight: wp(5) }}>
                        <Image
                            source={require('../img/refresh.png')}
                            style={{ height: hp(2.7), width: wp(5), }} />
                    </TouchableOpacity>
                </View>
                <ScrollView >
                    <View style={{ marginLeft: wp(7), marginTop: hp(3) }}>
                        <Text style={styles.headerText}>Price</Text>

                        <View style={styles.slider}>
                            <Text style={{
                                width: 50, textAlign: 'center', left: left,
                                color: Constant.color.black, fontSize: Constant.fontSize.xsmall, fontFamily: 'NexaDemo-Bold',
                            }}>
                                {`$${Math.floor(this.state.MultiSliderValue[0] + 50)}`}
                            </Text>
                            <Text style={{
                                width: 50, textAlign: 'center', left: left1,
                                color: Constant.color.black, fontSize: Constant.fontSize.xsmall, fontFamily: 'NexaDemo-Bold',
                            }}>
                                {`$${Math.floor(this.state.MultiSliderValue[1] + 50)}`}
                            </Text>
                        </View>

                        <View style={{ marginLeft: wp(5) }}>
                            <MultiSlider
                                values={[
                                    this.state.MultiSliderValue[0],
                                    this.state.MultiSliderValue[1],
                                ]}
                                onValuesChange={MultiSliderValue => this.setState({ MultiSliderValue })}
                                sliderLength={wp(80)}
                                min={0}
                                max={200}
                                step={10}
                                allowOverlap={false}
                                snapped
                                minMarkerOverlapDistance={40}
                                customMarker={CustomMarker}
                                
                                selectedStyle={{
                                    backgroundColor: Constant.color.background,
                                }}

                            />
                        </View>
                        <Text style={{ ...styles.headerText, marginTop: hp(2) }}>Sort By</Text>

                        <View style={{ marginTop: hp(1.5) }}>
                            <FlatList
                                data={SortData}
                                renderItem={(item, index) => this.renderSortItem(item, index)}
                                keyExtractor={item => item.name.toString()} />
                        </View>

                        <Text style={{ ...styles.headerText, marginTop: hp(2) }}>Cuisinies</Text>

                        <FlatList
                            data={CuisinData}
                            contentContainerStyle={styles.cuisin}
                            renderItem={item => this.renderCuisinItem(item)}
                            keyExtractor={item => item.name.toString()}
                        />

                    </View>

                    {this.state.cuisinValue.length != 0 || this.state.sortValue.length != 0 ?
                        <View style={{ alignItems: 'center', marginTop: hp(5), marginBottom: hp(3) }}>
                            <TouchableOpacity style={styles.button}>
                                <Text style={styles.text}>Apply Filter</Text>
                            </TouchableOpacity>
                        </View>
                        : null}
                </ScrollView>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    headerText: {
        color: Constant.color.black,
        fontSize: Constant.fontSize.medium,
        fontFamily: "NexaDemo-Bold"
    },
    seperateLine: {
        backgroundColor: Constant.color.lightGray,
        height: hp(0.1),
        marginTop: hp(1),
        marginBottom: hp(2),
        marginRight: wp(7)
    },
    cuisin: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: hp(1)
    },
    slider: {
        flexDirection: 'row',
        marginTop: hp(1.5),
        marginLeft: wp(4.2)
    },
    button: {
        borderRadius: wp(7),
        backgroundColor: Constant.color.background,
        width: wp(45),
        paddingVertical: 12,
        alignItems: 'center'
    },
    text: {
        fontSize: Constant.fontSize.medium,
        color: Constant.color.white,
        fontFamily: 'NexaDemo-Bold'
    },
});

export { FilterScreen }