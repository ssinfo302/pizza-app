import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, FlatList, ImageBackground, ScrollView } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import { drawer } from "../../App";

const MPData = [
    {
        image: require('../img/mp_11.png'),
        name: 'Cheezy Tomato',
        price: '$80.5',
        currentPrice: '$60',
        rate: 4
    },
    {
        image: require('../img/mp_22.png'),
        name: 'Spicy Corn',
        price: '$100',
        currentPrice: '$80',
        rate: 3
    },
    {
        image: require('../img/mp_33.png'),
        name: 'Margreta',
        price: '$130.5',
        currentPrice: '$120',
        rate: 4
    },
    {
        image: require('../img/mp_44.png'),
        name: '8‘s Corfin',
        price: '',
        currentPrice: '$80',
        rate: 3
    },
    {
        image: require('../img/mp_11.png'),
        name: 'Chili Flex',
        price: '$80.5',
        currentPrice: '$60',
        rate: 4
    },
    {
        image: require('../img/mp_22.png'),
        name: '13. ten',
        price: '$100',
        currentPrice: '$80',
        rate: 3
    },
];

// const MPData = [
//     {
//         image: require('../img/cp_3.png'),
//     },
//     {
//         image: require('../img/cp_4.png'),
//     },
//     {
//         image: require('../img/cp_3.png'),
//     },
//     {
//         image: require('../img/cp_4.png'),
//     },
// ];

class MostPopularScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            name: props.route.params && props.route.params.name ? props.route.params.name : ''
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        // const { name } = this.props.route.params;
        // this.setState({ name: name, })
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    renderMostPopularItem = (item, index) => {
        const starIcon = require('../img/star.png')
        const emptyIcon = require('../img/empty_star.png')

        let React_Native_Rating_Bar = []
        for (var i = 1; i <= 5; i++) {
            React_Native_Rating_Bar.push(
                <Image
                    style={styles.iconStyle}
                    source={i <= item.item.rate ? starIcon : emptyIcon}
                />
            )
        }
        return (
            <View style={{ marginRight: wp(4), width: wp(43), height: Platform.OS == 'ios' ? hp(33.5) : hp(32) }}>

                <ImageBackground
                    source={item.item.image}
                    style={{ height: Platform.OS == 'ios' ? hp(26) : hp(25), width: wp(43), }} >

                    <Text style={{ color: Constant.color.white, marginTop: hp(18.5), marginLeft: wp(3), fontSize: Constant.fontSize.xxsmall, fontFamily: "NexaDemo-Bold" }}>
                        {item.item.name}</Text>

                    <View style={{ flexDirection: 'row', marginLeft: wp(3), marginTop: hp(0.1) }}>{React_Native_Rating_Bar}</View>

                    <View style={{ flexDirection: 'row', marginTop: hp(0.5) }}>
                        <Text style={{ color: Constant.color.background, marginLeft: wp(3), fontSize: Constant.fontSize.xmini, fontFamily: "NexaDemo-Bold" }}>
                            {item.item.currentPrice}</Text>
                        <Text style={{ color: Constant.color.white, textDecorationLine: 'line-through', marginLeft: wp(2), fontSize: Constant.fontSize.xmini, fontFamily: "NexaDemo-Bold" }}>
                            {item.item.price}</Text>
                    </View>
                </ImageBackground>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderScreen', {
                        name: item.item.name,
                        image: item.item.image,
                        price: item.item.currentPrice
                    })}>
                        <View style={styles.button}>
                            <Text style={styles.text}>View</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    // renderMostPopularItem = (item, index) => {
    //     return (
    //         <View style={{ marginRight: wp(7), width: wp(40), height: hp(32) }}>

    //             <Image
    //                 source={item.item.image}
    //                 style={{ height: hp(25), width: wp(40) }} />

    //              <View style={{ alignItems: 'center' }}>
    //                 <TouchableOpacity >
    //                     <View style={styles.button}>
    //                         <Text style={styles.text}>View</Text>
    //                     </View>
    //                 </TouchableOpacity>
    //             </View>
    //         </View>
    //     )
    // }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.header}>
                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }}
                        onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5), }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{this.state.name === '' ? 'Most Popular' : 'Restaurant'} </Text>

                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0), right: 0, marginRight: wp(5) }}
                        onPress={() => this.props.navigation.navigate('FilterScreen')} >
                        <Image
                            source={require('../img/filter.png')}
                            style={{ height: hp(2.5), width: wp(5), }} />
                    </TouchableOpacity>
                </View>

                <View style={{ marginLeft: wp(5), marginRight: wp(5), marginTop: hp(2), marginBottom: hp(2), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                    <View style={{ flexDirection: 'row', height: hp(5) }}>
                        <TextInput
                            style={{ flex: 1, paddingLeft: wp(5), fontFamily: 'NexaDemo-Bold', color: Constant.color.lightGray, fontSize: Constant.fontSize.xxsmall, paddingRight: wp(14) }}
                            placeholder={'Search Your Food'}
                            placeholderTextColor={Constant.color.lightGray}
                            onChangeText={search => this.setState({ search })}
                        />
                        <View style={{ position: 'absolute', right: 0, flexDirection: 'row' }}>
                            <View style={styles.horizontalLine} />
                            <TouchableOpacity style={{ paddingTop: hp(1), paddingRight: wp(4) }} >
                                <Image source={require('../img/search.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ScrollView>
                    {this.state.name != '' ?
                        <View>
                            <ImageBackground
                                source={require('../img/restaurent.png')}
                                style={{ height: hp(30), width: '100%', }} >

                                <Text style={{
                                    color: Constant.color.white, marginTop: hp(23.5), marginLeft: wp(7),
                                    fontSize: Platform.OS == 'ios' ? Constant.fontSize.medium : Constant.fontSize.large, fontFamily: "NexaDemo-Bold"
                                }}> {this.state.name}</Text>

                                <View style={styles.seperateLine} />

                            </ImageBackground>

                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                                marginTop: hp(3), marginBottom:Platform.OS == 'ios' ? hp(0.7) : hp(1.5), marginLeft: wp(7), marginRight: wp(7)
                            }}>
                                <Text style={{ color: Constant.color.black, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xxsmall : Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    Call:-</Text>
                                <Text style={{ color: Constant.color.background, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xxsmall :Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    (+61) 456 - 12 7895</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                                marginBottom:Platform.OS == 'ios' ? hp(0.7) : hp(1.5), marginLeft: wp(7), marginRight: wp(7)
                            }}>
                                <Text style={{ color: Constant.color.black, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xxsmall :Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    Cuisinies:-</Text>
                                <Text style={{ color: Constant.color.background, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xxsmall :Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    Italian, Greek, Sicilian</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
                                marginBottom: hp(3), marginLeft: wp(7), marginRight: wp(7)
                            }}>
                                <Text style={{ color: Constant.color.black, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xxsmall :Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    Average Cost:-</Text>
                                <Text style={{ color: Constant.color.background, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xxsmall :Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    $50 - $250</Text>
                            </View>
                            <Text style={{ marginLeft: wp(7), color: Constant.color.black, fontSize: Platform.OS == 'ios' ? Constant.fontSize.xmedium :Constant.fontSize.medium, fontFamily: "NexaDemo-Bold" }}>
                                Testy Item</Text>
                        </View>
                        : null}

                    <View style={{ marginLeft: wp(5), flex: 1, marginBottom: hp(2), marginTop: Platform.OS == 'ios' ? hp(0.7) :hp(1.5) }}>
                        <FlatList
                            numColumns={2}
                            data={MPData}
                            renderItem={(item, index) => this.renderMostPopularItem(item, index)}
                            keyExtractor={item => item.image.toString()} />
                    </View>
                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    horizontalLine: {
        backgroundColor: Constant.color.lightGray,
        borderRadius: wp(3),
        width: wp(0.5),
        height: hp(3),
        marginRight: wp(3),
        marginTop: hp(1)
    },
    iconStyle: {
        width: wp(2),
        height: wp(2),
        resizeMode: 'cover',
        marginRight: wp(0.5)
    },
    button: {
        borderRadius: wp(8),
        borderColor: Constant.color.background,
        borderWidth: wp(0.5),
        width: wp(22),
        paddingVertical: wp(0.7),
        alignItems: 'center',
        marginTop: hp(1.2),
        marginBottom: hp(2)
    },
    text: {
        fontSize: Constant.fontSize.xxsmall,
        color: Constant.color.background,
        fontFamily: 'NexaDemo-Bold'
    },
    seperateLine: {
        backgroundColor: Constant.color.background,
        borderRadius: wp(1.5),
        width: wp(9),
        height: hp(0.3),
        marginTop: Platform.OS == 'ios' ? hp(0) :hp(0.5),
        marginLeft: wp(8)
    }
});

export { MostPopularScreen }