import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, Modal, FlatList, ScrollView } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import { drawer } from "../../App";

class PaymentProcessScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    renderItem = (item, index) => {
        var value = item.item.split(',', 2);

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: hp(1.5) }}>
                <Text style={styles.smallText}>
                    {`Extra ${value[0]}:-`}</Text>
                <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                    {`$${Number(value[1]).toFixed(2)}`}</Text>
            </View>
        )
    }

    render() {
        const { price, name, totalPrice, extraValue } = this.props.route.params;
        let dottedLine = []
        for (var i = 1; i <= 30; i++) {
            dottedLine.push(
                i <= 30 ? <View style={styles.seperateLine} /> : null

            )
        }

        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.header}>
                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }}
                        onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5) }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{'Payment'} </Text>

                </View>

                <ScrollView>
                    <View style={{ marginLeft: wp(7), marginTop: hp(2), marginRight: wp(7) }}>
                        <View style={{ alignItems: 'center' }}>
                            <Image
                                source={require('../img/credit_card.png')} />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: hp(2), marginBottom: hp(1.5) }}>
                            <Text style={styles.smallText}>{name}</Text>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                {` $${Number(price.replace("$", "")).toFixed(2)}`}</Text>
                        </View>

                        <FlatList
                            data={extraValue}
                            renderItem={(item, index) => this.renderItem(item, index)} />

                        <View style={{ flexDirection: 'row' }}>
                            {dottedLine}
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: hp(1.5) }}>
                            <Text style={styles.text}>Total:-</Text>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                {` $${Number(totalPrice).toFixed(2)}`}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: hp(1.5) }}>
                            <Text style={styles.text}>Tax & fees:-</Text>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                {` $5.00`}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: hp(6) }}>
                            <Text style={styles.text}>Delivery</Text>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                Free</Text>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                            <Text style={styles.text}>Sub Total:-</Text>
                            <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                {` $${Number(parseInt(totalPrice) + 5).toFixed(2)}`}</Text>
                        </View>

                        <View style={{ marginTop: hp(3), marginBottom: hp(6) }}>
                            <TouchableOpacity style={styles.button} onPress={() => this.setState({ isModalVisible: true })}>
                                <Text style={styles.btnText}>Process</Text>
                            </TouchableOpacity>
                        </View>


                    </View>
                </ScrollView>

                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.isModalVisible}>
                    <View style={styles.model}>
                        <View style={{ borderRadius: wp(6), backgroundColor: Constant.color.white, alignItems: 'center' }}>
                            <Image
                                source={require('../img/successfull.png')}
                                style={{ marginTop: hp(3), width: wp(30), height: wp(30) }} />

                            <Text style={{
                                marginLeft: wp(12), marginRight: wp(12), marginTop: hp(3), textAlign: 'center', color: Constant.color.black,
                                fontSize: Constant.fontSize.xmedium, fontFamily: "NexaDemo-Bold"
                            }}>{`Your Order is Successfully \n Completed `}</Text>

                            <View style={{ marginTop: hp(6), marginBottom: hp(4) }}>
                                <TouchableOpacity style={styles.button}
                                    onPress={() => { this.setState({ isModalVisible: false }), this.props.navigation.navigate('DeliciousScreen') }}>
                                    <Text style={{ fontSize: Constant.fontSize.xxmedium, color: Constant.color.white, fontFamily: 'NexaDemo-Bold' }}>
                                        Continue Shopping</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    smallText: {
        color: Constant.color.black,
        fontSize: Constant.fontSize.xsmall,
        fontFamily: "NexaDemo-Bold",
    },
    text: {
        color: Constant.color.black,
        fontSize: Constant.fontSize.xxmedium,
        fontFamily: "NexaDemo-Bold",
    },
    button: {
        borderRadius: wp(7),
        backgroundColor: Constant.color.background,
        paddingHorizontal: 30,
        paddingVertical: 12,
        alignItems: 'center'
    },
    btnText: {
        fontSize: Constant.fontSize.medium,
        color: Constant.color.white,
        fontFamily: 'NexaDemo-Bold'
    },
    seperateLine: {
        backgroundColor: Constant.color.lightGray,
        height: hp(0.25),
        width: wp(1.9),
        marginTop: hp(1),
        marginBottom: hp(2),
        marginRight: wp(3),
        marginTop: hp(1.5),
        marginBottom: hp(4)
    },
    model: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rrgba(0, 0, 0, 0.75)',
    },
});

export { PaymentProcessScreen }