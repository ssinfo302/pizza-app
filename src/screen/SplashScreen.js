import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, StatusBar } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'

class SplashScreen extends Component {

    async componentDidMount() {

        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)

        setTimeout(() => {
            this.props.navigation.navigate('OnboardingScreen')
        }, 1000);

    }


    render() {
        return (
            <View style={styles.conatinar}>
                <Image source={require('../img/logo.png')} style={styles.LogoImg} resizeMode={'contain'} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    LogoImg: {
        width: 270,
        height: 270
    },
    conatinar: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Constant.color.background
    }
})

export { SplashScreen }