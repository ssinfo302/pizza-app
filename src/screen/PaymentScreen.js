import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, FlatList, ScrollView } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import { drawer } from "../../App";

const Data = [
    {
        image: require('../img/visa.png'),
        name: 'Visa',
    },
    {
        image: require('../img/mastercard.png'),
        name: 'Master Card',
    },
    {
        image: require('../img/cash.png'),
        name: 'Cash on Delivery',
    },
    {
        image: require('../img/cards.png'),
        name: 'Local Debit card',
    },
];


class PaymentScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectPayment: ''
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    renderItem = (item, index) => {
        return (
            <View>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}
                    onPress={() => this.setState({ selectPayment: item.item.name })}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Image
                            source={item.item.image}
                            style={{ height: wp(8), width: wp(8) }} />
                        <Text style={styles.text}>{item.item.name}</Text>
                    </View>
                    {this.state.selectPayment == item.item.name ?
                        <Image
                            source={require('../img/marker.png')}
                            style={{ marginRight: wp(2) }} />
                        : null}
                </TouchableOpacity>
                <View style={styles.seperateLine} />
            </View>
        )
    }

    render() {
        const { price, name, totalPrice, extraValue } = this.props.route.params;
        // console.log('value', price + ',' + name+ ',' + totalPrice+ ',' + extraValue)
        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.header}>
                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }}
                        onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5) }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{'Payment'} </Text>

                </View>

                <View style={{ marginLeft: wp(7), marginTop: hp(4), marginRight: wp(7) }}>
                    <FlatList
                        data={Data}
                        renderItem={(item, index) => this.renderItem(item, index)}
                        keyExtractor={item => item.name.toString()} />

                    {this.state.selectPayment != '' ?
                        <View style={{ marginTop: hp(7), marginBottom: hp(3) }}>
                            <TouchableOpacity style={styles.button}
                             onPress={() => this.props.navigation.navigate('PaymentProcessScreen', {
                                name: name,
                                price: price,
                                totalPrice: totalPrice,
                                extraValue: extraValue
                            })}>
                                <Text style={styles.btnText}>Payment</Text>
                            </TouchableOpacity>
                        </View>
                        : null}

                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    text: {
        color: Constant.color.black,
        fontSize: Constant.fontSize.small,
        fontFamily: "NexaDemo-Bold",
        marginLeft: wp(6)
    },
    button: {
        borderRadius: wp(7),
        backgroundColor: Constant.color.background,

        paddingVertical: 12,
        alignItems: 'center'
    },
    btnText: {
        fontSize: Constant.fontSize.medium,
        color: Constant.color.white,
        fontFamily: 'NexaDemo-Bold'
    },
    seperateLine: {
        backgroundColor: Constant.color.lightGray,
        height: hp(0.1),
        marginTop: hp(3),
        marginBottom: hp(3),
    },
});

export { PaymentScreen }