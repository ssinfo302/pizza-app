import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, FlatList, ScrollView } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import { drawer } from "../../App";

const Data = [
    {
        image: require('../img/onion.png'),
        name: 'Onion',
        price: 5
    },
    {
        image: require('../img/mushroom.png'),
        name: 'Mushroom',
        price: 10
    },
    {
        image: require('../img/pepper.png'),
        name: 'Pepper',
        price: 15
    },
    {
        image: require('../img/tomato.png'),
        name: 'Tomato',
        price: 20
    },
    {
        image: require('../img/sausages.png'),
        name: 'Sausages',
        price: 25
    },
    {
        image: require('../img/olive.png'),
        name: 'Olive',
        price: 30
    },
];


class OrderScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            extraValue: [],
            price: '',
            name: '',
            image: '',
            quantity: 1,
            originalPrice: 0,
            extraPrice: 0
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        const { price, name, image } = this.props.route.params;
        this.setState({ price: price.replace("$", ""), name: name, image: image, originalPrice: price.replace("$", "") })

        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content', true)
        StatusBar.setBackgroundColor(Constant.color.background)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    renderItem = (item, index) => {
        return (
            <TouchableOpacity style={{ marginRight: wp(7), alignItems: "center" }}
                onPress={() => this.onPressItem(item.item.name, item.item.price)}>
                <Image
                    source={item.item.image}
                    style={{ height: Platform.OS == 'ios' ? Constant.isX ? hp(4.3) : hp(5.3) : wp(9), width: Platform.OS == 'ios' ? wp(9) : wp(9) }} />
                <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxsmall, fontFamily: 'NexaDemo-Bold', marginTop: hp(1) }}>
                    {item.item.name}</Text>

                {this.state.extraValue.length != 0 &&
                    this.state.extraValue.some(function (el) {
                        return el === `${item.item.name},${item.item.price}`
                    })
                    ?
                    <View style={styles.selectedLine} />
                    : null}
            </TouchableOpacity>
        )
    }

    onPressItem(value, price) {

        let exist = this.state.extraValue.some(function (el) {
            return el === `${value},${price}`
        })
        if (!exist) {
            var total = parseInt(this.state.price) + price
            this.setState({ price: total, extraPrice: this.state.extraPrice + price })
            this.setState(prevState => ({
                extraValue: [...prevState.extraValue, `${value},${price}`]
            }));
            this.forceUpdate()
        } else {
            var total = parseInt(this.state.price) - price
            this.setState({ price: total, extraPrice: this.state.extraPrice - price })
            var removeIndex = this.state.extraValue
                .map(function (item) {
                    return item
                })
                .indexOf(`${value},${price}`)
            this.state.extraValue.splice(removeIndex, 1)
            this.forceUpdate()
        }
    }

    renderExtraItem = (item, index) => {
        var value = item.item.split(',', 2);

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: hp(2) }}>
                <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                    {`Extra ${value[0]}:-`}</Text>
                <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                    {`$${Number(value[1]).toFixed(2)}`}</Text>
            </View>
        )
    }

    render() {
        const { price, name } = this.props.route.params;
        let dottedLine = []
        for (var i = 1; i <= 30; i++) {
            dottedLine.push(
                i <= 30 ? <View style={styles.seperateLine} /> : null

            )
        }
        // this.setState({price: parseInt(this.state.price) * this.state.quantity})

        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.header}>
                    <TouchableOpacity
                        style={{ position: 'absolute', paddingTop: Platform.OS == 'ios' ? hp(3) : hp(0), left: 0, marginLeft: wp(5) }}
                        onPress={() => drawer.current.open()}>
                        <Image
                            source={require('../img/menu.png')}
                            style={{ height: hp(2), width: wp(5) }} />
                    </TouchableOpacity>

                    <Text style={{
                        color: Constant.color.white,
                        fontSize: Constant.fontSize.large,
                        fontFamily: 'NexaDemo-Bold',
                    }}>{'Order'} </Text>

                </View>

                <ScrollView>
                    <View style={{ marginLeft: wp(5) }}>

                        <View style={{ alignItems: 'center', marginTop: hp(3) }}>


                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: "center" }}>
                                <View style={{
                                    marginBottom: hp(2),
                                    borderRadius: wp(20),
                                    backgroundColor: '#DCDCDC',
                                    width: wp(9),
                                    paddingVertical: Platform.OS == 'ios' ? hp(0.2) : hp(0.8),
                                    alignItems: 'center',
                                    position: 'absolute',
                                    right: 0,
                                    top: Platform.OS == 'ios' ? hp(4) : hp(5),
                                    marginRight: wp(24),
                                }}>
                                    <Text style={{ fontSize: Constant.fontSize.medium, color: Constant.color.white, fontFamily: 'NexaDemo-Bold' }}>
                                        S</Text>
                                </View>

                                <View style={{
                                    marginBottom: Platform.OS == 'ios' ? hp(1.2) : hp(2),
                                    borderRadius: wp(20),
                                    backgroundColor: Constant.color.background,
                                    width: wp(9),
                                    paddingVertical: Platform.OS == 'ios' ? hp(0.2) : hp(0.8),
                                    alignItems: 'center',
                                }}>
                                    <Text style={{ fontSize: Constant.fontSize.medium, color: Constant.color.white, fontFamily: 'NexaDemo-Bold' }}>
                                        M</Text>
                                </View>

                                <View style={{
                                    marginBottom: hp(2),
                                    borderRadius: wp(20),
                                    backgroundColor: '#DCDCDC',
                                    width: wp(9),
                                    paddingVertical: Platform.OS == 'ios' ? hp(0.2) : hp(0.8),
                                    alignItems: 'center',
                                    position: 'absolute',
                                    left: 0,
                                    top: Platform.OS == 'ios' ? hp(4) : hp(5),
                                    marginLeft: wp(24),
                                }}>
                                    <Text style={{ fontSize: Constant.fontSize.medium, color: Constant.color.white, fontFamily: 'NexaDemo-Bold' }}>
                                        L</Text>
                                </View>

                            </View>

                            <View style={{
                                borderTopColor: Constant.color.background,
                                borderLeftColor: Constant.color.white,
                                borderRightColor: Constant.color.white,
                                borderTopWidth: hp(0.6),
                                borderLeftWidth: hp(0.6),
                                borderRightWidth: hp(0.6),
                                width: Platform.OS == 'ios' ? wp(70) : wp(70),
                                height: Platform.OS == 'ios' ? wp(70) : wp(37),
                                borderTopLeftRadius: wp(35), borderTopRightRadius: wp(35),

                            }}></View>

                            <Image
                                source={require('../img/order_image.png')}
                                style={{ width: wp(50), height: wp(50), marginTop: Platform.OS == 'ios' ? -wp(65) : -wp(33), borderRadius: wp(25) }} />
                            <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.medium, fontFamily: "NexaDemo-Bold", marginTop: hp(1.5) }}>
                                {this.state.name}</Text>

                            <View style={{
                                marginTop: hp(2),
                                borderRadius: wp(7),
                                backgroundColor: Constant.color.background,
                                width: wp(28),
                                paddingVertical: hp(1),
                                alignItems: 'center'
                            }}>
                                <TouchableOpacity style={{
                                    marginTop: hp(1),
                                    borderRadius: wp(7),
                                    backgroundColor: Constant.color.white,
                                    width: wp(6.5),
                                    paddingVertical: Platform.OS == 'ios' ? hp(0) : hp(0.5),
                                    alignItems: 'center',
                                    position: 'absolute',
                                    left: wp(1.2),
                                    bottom: hp(0.6),
                                }}
                                    onPress={() => this.state.quantity != 1 ?
                                        this.setState({
                                            quantity: this.state.quantity - 1,
                                            price: this.state.extraPrice + (this.state.originalPrice * (this.state.quantity - 1))
                                        }) : null}>

                                    <Text style={{ fontSize: Constant.fontSize.small, color: Constant.color.background, fontFamily: 'NexaDemo-Bold' }}>
                                        -</Text>

                                </TouchableOpacity>

                                <View style={{ width: wp(5), height: wp(5), backgroundColor: '#FFFFFF30', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{
                                        fontSize: Constant.fontSize.xsmall,
                                        color: Constant.color.white,
                                        fontFamily: 'NexaDemo-Bold',

                                    }}>{this.state.quantity}</Text>
                                </View>

                                <TouchableOpacity style={{
                                    marginTop: hp(1),
                                    borderRadius: wp(7),
                                    backgroundColor: Constant.color.white,
                                    width: wp(6.5),
                                    paddingVertical: Platform.OS == 'ios' ? hp(0) : hp(0.5),
                                    alignItems: 'center',
                                    position: 'absolute',
                                    right: wp(1.2),
                                    bottom: hp(0.6),
                                }}
                                    onPress={() =>
                                        this.setState({
                                            quantity: this.state.quantity + 1,
                                            price: this.state.extraPrice + (this.state.originalPrice * (this.state.quantity + 1))
                                        })}>
                                    <Text style={{ fontSize: Constant.fontSize.small, color: Constant.color.background, fontFamily: 'NexaDemo-Bold' }}>
                                        +</Text>
                                </TouchableOpacity>

                            </View>
                        </View>

                        <Text style={{ ...styles.headerText, marginTop: hp(3) }}>Ingregients</Text>

                        <View style={{ marginTop: hp(3) }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={Data}
                                renderItem={(item, index) => this.renderItem(item, index)}
                                keyExtractor={item => item.name.toString()} />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            {dottedLine}
                        </View>

                        <View style={{ marginRight: wp(7), marginLeft: wp(2) }}>
                            <FlatList
                                data={this.state.extraValue}
                                renderItem={(item, index) => this.renderExtraItem(item, index)} />
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: hp(1) }}>
                                <Text style={styles.headerText}>Total:-</Text>
                                <Text style={{ color: Constant.color.background, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                                    {`$${Number(this.state.price).toFixed(2)}`}</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: hp(5), marginBottom: hp(6) }}>
                            <TouchableOpacity style={styles.button}
                                onPress={() => this.props.navigation.navigate('PaymentScreen', {
                                    name: name,
                                    price: price,
                                    totalPrice: this.state.price,
                                    extraValue: this.state.extraValue
                                })}>
                                <Text style={styles.text}>Conf<Text style={styles.text}>irm</Text></Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0),
        height: Platform.OS == 'ios' ? hp(11) : hp(8),
        backgroundColor: Constant.color.background
    },
    headerText: {
        color: Constant.color.black,
        fontSize: Constant.fontSize.medium,
        fontFamily: "NexaDemo-Bold"
    },
    selectedLine: {
        backgroundColor: Constant.color.background,
        borderRadius: wp(1.5),
        width: wp(7),
        height: hp(0.3),
        marginTop: hp(0.3),
    },
    seperateLine: {
        backgroundColor: Constant.color.lightGray,
        height: hp(0.25),
        width: wp(2),
        marginTop: hp(1),
        marginBottom: hp(2),
        marginRight: wp(3),
        marginTop: hp(5),
        marginBottom: hp(3)
    },
    button: {
        borderRadius: wp(7),
        backgroundColor: Constant.color.background,
        marginLeft: wp(2),
        marginRight: wp(7),
        paddingVertical: 12,
        alignItems: 'center'
    },
    text: {
        fontSize: Constant.fontSize.medium,
        color: Constant.color.white,
        fontFamily: 'NexaDemo-Bold'
    },
});

export { OrderScreen }