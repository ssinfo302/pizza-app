import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, CheckBox } from 'react-native';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import Button from '../component/Button';

class VerifyPhoneScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mobileNo: '9977664422',
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('dark-content', true)
        StatusBar.setBackgroundColor(Constant.color.white)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.conatinarLeft}>
                    <Image source={require('../img/left_side_pizza.png')} style={styles.Img} resizeMode={'contain'} />
                </View>
                <View style={styles.conatinarRight}>
                    <Image source={require('../img/right_side_pizza.png')} style={styles.Img} resizeMode={'contain'} />
                </View>
                <View style={{ marginTop: Platform.OS == 'ios' ? hp(21) : hp(17), marginBottom: hp(1), alignItems: 'center', }}>
                    <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxlarge, fontFamily: "NexaDemo-Bold" }}>
                        Verify Phone</Text>
                    <View style={styles.seperateLine} />

                    <Text style={{ marginTop: hp(4), textAlign: 'center', color: Constant.color.black, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                        {`We Will Send you otp on \n this mobile number `} </Text>
                </View>



                <View style={{ margin: wp(8), borderRadius: wp(8), borderWidth: wp(0.3), borderColor: Constant.color.lightGray }}>
                    <View style={{ flexDirection: 'row', padding: Platform.OS == 'ios' ? wp(4) : 0 }}>
                        <View style={{ marginLeft: Platform.OS == 'ios' ? wp(18): wp(25), justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'NexaDemo-Bold', color: Constant.color.background, fontSize: Constant.fontSize.small }}>+91 - </Text>
                        </View>

                        <TextInput
                            style={{ flex: 1, marginLeft: Platform.OS == 'ios' ? wp(-18): wp(-25), fontFamily: 'NexaDemo-Bold', color: Constant.color.background, fontSize: Constant.fontSize.small }}
                            placeholderTextColor={Constant.color.background}
                            value={this.state.mobileNo}
                            keyboardType={'numeric'}
                            maxLength={10}
                            textAlign={'center'}
                            onChangeText={mobileNo => this.setState({ mobileNo })}
                        />

                    </View>
                </View>

                <View style={{ alignItems: 'center', marginTop: hp(2) }}>
                    <Button text="Get Otp" onPress={() => this.props.navigation.navigate('VerificationScreen', { MobileNo: this.state.mobileNo })} />
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    ImgLeft: {
        width: wp(35),
        height: hp(25)
    },
    conatinarLeft: {
        flex: 1,
        position: 'absolute',
        left: 0,
        marginTop: Platform.OS == 'ios' ? hp(5.5) : hp(1.5)
    },
    ImgRight: {
        width: wp(25),
        height: hp(25)
    },
    conatinarRight: {
        flex: 1,
        position: 'absolute',
        right: 0,
        marginTop: Platform.OS == 'ios' ? hp(7.5) : hp(3.5)
    },
    seperateLine: {
        backgroundColor: Constant.color.background,
        borderRadius: wp(3),
        width: wp(13),
        height: hp(0.5),
        marginTop: hp(0.5),
        marginRight: wp(30)
    }
});

export { VerifyPhoneScreen }