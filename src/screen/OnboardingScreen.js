import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler } from 'react-native';
import Swiper from '../component/Swiper';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'

class OnboardingScreen extends Component {

    componentDidMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('dark-content', true)
        StatusBar.setBackgroundColor(Constant.color.white)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        BackHandler.exitApp()
        return true
    }

    render() {
        const { navigation } = this.props;
        return (
            <Swiper navigation={navigation}>
                {/* First screen */}
                <View style={styles.slide}>

                    <Text style={{
                        color: Constant.color.text, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold",
                        marginTop: Platform.OS == 'ios' ? hp(8) : hp(4), position: 'absolute', right: wp(5),
                    }}>Skip</Text>

                    <Image source={require('../img/screen1.png')} style={{ ...styles.Img, marginTop: hp(18) }} resizeMode={'contain'} />
                    <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xlarge, fontFamily: "NexaDemo-Bold", marginTop: hp(4) }}>
                        Be Togather</Text>
                    <View style={{ flex: 1, flexDirection: 'column', marginTop: hp(3) }}>
                        <Text style={{ textAlign: 'center', color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {`Healthy eating means eating a variety of `} </Text>
                        <Text style={{ textAlign: 'center', marginTop: hp(0.5), color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold" }}>
                            {`foods that give you the nutrients you need `}</Text>
                        <Text style={{ textAlign: 'center', marginTop: hp(0.5), color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {` to maintain your health, feel good, and `}</Text>
                        <Text style={{ textAlign: 'center', marginTop: hp(0.5), color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {` have energy.`}</Text>
                    </View>

                </View>
                {/* Second screen */}
                <View style={styles.slide}>

                    <Text style={{
                        color: Constant.color.text, fontSize: Constant.fontSize.small, fontFamily: "NexaDemo-Bold",
                        marginTop: Platform.OS == 'ios' ? hp(8) : hp(4), position: 'absolute', right: wp(5),
                    }}>Skip</Text>

                    <Image source={require('../img/screen2.png')} style={{ ...styles.Img, marginTop: hp(18) }} resizeMode={'contain'} />
                    <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xlarge, fontFamily: 'NexaDemo-Bold', marginTop: hp(4) }}>
                        Choose A Tasty Dish</Text>
                    <View style={{ flex: 1, flexDirection: 'column', marginTop: hp(3) }}>
                        <Text style={{ textAlign: 'center', color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {`Order anything you want from your`}</Text>
                        <Text style={{ textAlign: 'center', marginTop: hp(0.5), color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {`Favorite restaurant.`}</Text>
                    </View>
                </View>
                {/* Third screen */}
                <View style={styles.slide}>
                    <Image source={require('../img/screen3.png')} style={{ ...styles.Img, marginTop: hp(18) }} resizeMode={'contain'} />
                    <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xlarge, fontFamily: 'NexaDemo-Bold', marginTop: hp(4) }}>
                        Easy Payment</Text>
                    <View style={{ flex: 1, flexDirection: 'column', marginTop: hp(3) }}>
                        <Text style={{ textAlign: 'center', color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {`Payment made easy through debit card,`}</Text>
                        <Text style={{ textAlign: 'center', marginTop: hp(0.5), color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {`credit card & more ways to pay for your`}</Text>
                        <Text style={{ textAlign: 'center', marginTop: hp(0.5), color: Constant.color.text, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                            {`food`}</Text>
                    </View>
                </View>
            </Swiper>
        )
    }

}

const styles = StyleSheet.create({
    // Slide styles
    slide: {
        flex: 1,                    // Take up all screen
        alignItems: 'center',
        backgroundColor: Constant.color.white,
        paddingTop: Platform.OS == 'ios' ? hp(4) : hp(0)
    },
    Img: {
        width: '85%',
        height: hp(30),
    },
});

export { OnboardingScreen }