import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Platform, TouchableOpacity, BackHandler, TextInput, CheckBox } from 'react-native';
import Swiper from '../component/Swiper';
import Constant from '../helper/themeHelper'
import { wp, hp } from '../helper/responsiveScreen'
import Button from '../component/Button';
import OTPTextView from 'react-native-otp-textinput';

class VerificationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mobileNo: '',
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    }

    componentDidMount() {
        const { MobileNo } = this.props.route.params;
        this.setState({ mobileNo: MobileNo })

        StatusBar.setHidden(false);
        StatusBar.setBarStyle('dark-content', true)
        StatusBar.setBackgroundColor(Constant.color.white)
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.handleBackButtonClick,
        )
    }

    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.white }}>
                <View style={styles.conatinarLeft}>
                    <Image source={require('../img/left_side_pizza.png')} style={styles.Img} resizeMode={'contain'} />
                </View>
                <View style={styles.conatinarRight}>
                    <Image source={require('../img/right_side_pizza.png')} style={styles.Img} resizeMode={'contain'} />
                </View>
                <View style={{ marginTop: Platform.OS == 'ios' ? hp(21) : hp(17), marginBottom: hp(1), alignItems: 'center', }}>
                    <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxlarge, fontFamily: "NexaDemo-Bold" }}>
                        {`Verif`}<Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xxlarge, fontFamily: "NexaDemo-Bold" }}>
                            {`ication`}</Text></Text>
                    <View style={styles.seperateLine} />

                    <Text style={{ marginTop: hp(4), textAlign: 'center', color: Constant.color.black, fontSize: Constant.fontSize.xsmall, fontFamily: "NexaDemo-Bold", }}>
                        {`Enter the otp sent to \n +91 - ${this.state.mobileNo}`} </Text>
                </View>

                <View style={{ alignItems: 'center', marginTop: hp(2) }}>
                <OTPTextView
                    handleTextChange={(e) => { }}
                    containerStyle={styles.textInputContainer}
                    textInputStyle={[styles.roundedTextInput, { borderRadius: 100 }]}
                    tintColor={Constant.color.background}
                />
                </View>


                <View style={{ alignItems: 'center', marginTop: hp(2) }}>
                    <Button text="Verify" onPress={() => this.props.navigation.navigate('DeliciousScreen')} />
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    ImgLeft: {
        width: wp(35),
        height: hp(25)
    },
    conatinarLeft: {
        flex: 1,
        position: 'absolute',
        left: 0,
        marginTop: Platform.OS == 'ios' ? hp(5.5) : hp(1.5)
    },
    ImgRight: {
        width: wp(25),
        height: hp(25)
    },
    conatinarRight: {
        flex: 1,
        position: 'absolute',
        right: 0,
        marginTop: Platform.OS == 'ios' ? hp(7.5) : hp(3.5)
    },
    seperateLine: {
        backgroundColor: Constant.color.background,
        borderRadius: wp(3),
        width: wp(13),
        height: hp(0.5),
        marginTop: hp(0.5),
        marginRight: wp(25)
    },
    textInputContainer: {
        marginBottom: hp(3),
        width: '70%'
    },
    roundedTextInput: {
        borderRadius: 10,
        // borderWidth: 3,
        borderTopWidth:1.5,
        borderLeftWidth:1.5,
        borderRightWidth:1.5,
        borderBottomWidth:1.5,
        color: Constant.color.background,
        fontSize: Constant.fontSize.xsmall,
        borderColor: Constant.color.lightGray, 
        fontFamily: "NexaDemo-Bold"
    },
});

export { VerificationScreen }