import {Platform, Dimensions, PixelRatio} from 'react-native'

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window')

const isIOS = Platform.OS === 'ios'
const d = Dimensions.get('window')

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 375

export function normalize (size) {
  const newSize = size * scale
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}
module.exports = {
  //API Constant
  color: {
    background: '#F4313F',
    blue: '#1D67B4',
    navyBlue: '#305489',
    darkBlue: '#093C72',
    lightBlue: '#2D80D7',
    skyBlue: '#0AB6F3',
    sky: '#9DCDFF',
    lightSky: '#E3F0FF',
    darkGray: '#B3B3B3',
    gray: '#C5C5C5',
    lightGray: '#18233550',
    error: '#D15A58',
    green: '#41B2A9',
    lightyellow: '#FFD69F',
    blackColor: '#02152a',
    white: '#ffffff',
    header: '#054993',
    text: '#B06E6E',
    button: '#00cb6f3',
    // blue:"#054993",
    lightblue: '#0cb6f3',
    facebook: '#3667b8',
    twitter: '#00a3f9',
    instragram: '#e50069',
    textInput: '#0a1d41',
    transparent: 'transparent',
    transparentWhite: '#FFFFFF80',
    black: '#182335',
    sepratorColor: '#E3F0FF',
  },
  font: {
    // Nunito_Regular: isIOS && 'Nunito-Regular' || 'Nunito_Regular',
    // Nunito_Bold: isIOS && 'Nunito-Bold' || 'Nunito_Bold',
  },
  style: {
    container: {width: SCREEN_WIDTH * 0.85, alignSelf: 'center'},
  },
  screen: Dimensions.get('window'),
  isIOS: isIOS,
  isANDROID: Platform.OS === 'android',
  isiPAD: SCREEN_HEIGHT / SCREEN_WIDTH < 1.6,
  isX:
    Platform.OS === 'ios' && (d.height > 800 || d.width > 800) ? true : false,

  screenHeight: (isIOS && SCREEN_HEIGHT) || SCREEN_HEIGHT - 24,
  screenWidth: SCREEN_WIDTH,
  fullScreenHeight: SCREEN_HEIGHT,

  fontSize: {
    xxxmini: normalize(9),
    xxmini: normalize(10),
    xmini: normalize(11),
    mini: normalize(12),
    xxxsmall: normalize(13),
    xxsmall: normalize(14),
    xsmall: normalize(15),
    small: normalize(16),
    xxmedium: normalize(17),
    xmedium: normalize(18),
    medium: normalize(20),
    large: normalize(22),
    xlarge: normalize(24),
    xxlarge: normalize(28),
  },

  shadowStyle: {
    shadowColor: '#E3F0FF',
    shadowOffset: {width: 1, height: 2},
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 2,
    zIndex: 10000000,
  },

  gradientColours: {
    green: ['transparent', '#245250'],
    darkGreen: ['transparent', '#0D4354'],
    blue: ['transparent', '#48B4FF'],
    lightBlue: ['transparent', '#2D80D7'],
    darkBlue: ['transparent', '#093C72'],
  },
}
